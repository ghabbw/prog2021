/**
TP02-EJ03- Utilice la librer�a <stdlib.h> y <time.h> para crear un programa que simule la espera de N clientes
en una cola para hacer un tr�mite en el banco. Para cada uno de los N clientes que llegan al banco, se asigna
el tiempo de espera de forma aleatoria, siendo un n�mero natural en el rango [1,30]. El programa debe calcular:
a) Promedio total de espera
b) El tiempo de la menor espera
c) El tiempo de la mayor espera
**/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 30
#define MIN 1
int genera_tiempo_azar(int, int);

int main(void)
{
    int i, N, tiempo_espera, acum, menor_esp, mayor_esp;
    srand(time(NULL));
    
    acum = 0, menor_esp = MAX + 1, mayor_esp = MIN - 1;
    printf("\n Ingrese la cantidad de clientes:  ");
    scanf("%d", &N);
    for(i = 1; i <= N; i++)
    {
        tiempo_espera = genera_tiempo_azar(MAX, MIN);
        printf("\n Tiempo de espera: ______%d   <%d/%d>", tiempo_espera, i, N);
        acum = acum + tiempo_espera;
        if(tiempo_espera > mayor_esp)
        {
            mayor_esp = tiempo_espera;
        }
        if(tiempo_espera < menor_esp)
        {
            menor_esp = tiempo_espera;
        }
    }
    printf("\n Tiempo promedio de espera: %.3f", acum / (float)N);
    printf("\n Mayor tiempo de espera: %d", mayor_esp);
    printf("\n Menor tiempo de espera: %d", menor_esp);
    return 0;
}
int genera_tiempo_azar(int tope, int piso)
{
    int salida;
    salida = rand() % tope + piso;
    return salida;
}
