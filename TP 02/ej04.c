/**
TP02-EJ04- Realizar un programa que genere una lista de N n�meros aleatorios en el rango [A, B] (con A y B
naturales) y muestre aquellos n�meros aleatorios que cumplan con NO ser deficientes.
**/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int suma_divs(int);
int genera_aleatorio(int, int);

int main(void)
{
    int i, N, num_rand, A, B;
    srand(time(NULL));
    printf("\n Ingrese el tamanho de la lista:   ");
    scanf("%d", &N);
    printf("\n Ingrese el piso de los numeros aleatorios:   ");
    scanf("%d", &A);
    printf("\n Ingrese el techo de los numeros aleatorios:   ");
    scanf("%d", &B);
    for(i = 1; i <= N; i++)
    {
        num_rand = genera_aleatorio(A, B);
        if(suma_divs(num_rand) >= num_rand)
        {
            printf("\n Numeros generados no deficientes: %d", num_rand);
        }
        
    }
    
    return 0;
}
int genera_aleatorio(int a, int b)
{   /** numero = rand () % (mayor - menor + 1) + menor; **/
    int salida;
    salida = rand() % (b - a + 1) + a;
    return salida;
}
int suma_divs(int nat)
{
    int pd, acum;
    pd = 1, acum = 0;
    while(pd <= nat / 2)
    {
        if(nat % pd == 0)
        {
            acum = acum + pd;
        }
        pd++;
    }
    return acum;
}
