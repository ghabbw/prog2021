/**
TP02-EJ07- Dada N fechas ingresadas por el usuario, para cada una de ellas genere una fecha al azar mayor
a esta y luego indique la cantidad de d�as transcurridos entre la fecha ingresada y la fecha generada al azar.
Tambi�n mostrar la cantidad promedio de d�as transcurridos.
**/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long int calc2f (int, int, int, int, int, int);
int diarandom (void);

int main(void)
{
    float prom;
    int i, N, lim, diaR, mesR, anhoR, dia, mes, anho;
    long int cant_dias, acum;
    
    srand(time(NULL));
    acum = 0;
    do  /* do:Evita que N sea 0 como divisor */
    {
        printf("\n Cantidad de fechas a computar...        **Distinta de cero**\n");
        scanf("%d", &N);
    }while(N <= 0);
    printf("\n Ingrese una fecha\n");
    for(i = 1; i <= N; i++)
    {
        do
        {
            printf("\n Ingrese el anho...           **Entre 1 y 2500**\n");
            scanf("%d", &anho);
        }while(anho <= 0 || anho >= 2500);
        do
        {
            printf("\n Ingrese el mes    ");
            scanf("%d", &mes);
        }while(mes < 1 || mes > 12);
        switch(mes)
        {
            case 2 :
                lim = 28;
            break;
            case 4 : case 6 : case 9 : case 11 :
                lim = 30;
            break;
            default :
                lim = 31;
        }
        do
        {
            printf("\n Ingrese el dia\n");
            scanf("%d", &dia);
        }while(dia < 1 || dia > lim);
        do
        {
            mesR = rand() % 12 + 1; /* Da un mes random */
            switch(mesR)
            {
                case 2 :
                    diaR = diarandom(28);   /* da dia random dependiendo del mes */
                break;
                case 4 : case 6 : case 9 : case 11 :
                    diaR = diarandom(30);
                break;
                default :
                    diaR = diarandom(31);
            }
            anhoR = rand() % 2500 + 1;  /* Da un a�o random */
            cant_dias = calc2f(dia, diaR, mes, mesR, anho, anhoR);
        }while(cant_dias < 0);
        printf("\n\n  Fecha generada al azar (1<anho<2500)... (%d / %d / %d) \n",diaR,mesR,anhoR);
        acum = acum + cant_dias;
    }
    prom = acum / (float)N; /* convierto a float para sacar promedio */
    printf("\n El promedio de dias es:  %f ", prom);
    return 0;
}
int diarandom (int tope)
{
    return rand() % tope + 1;
}
long int calc2f (int dia,int diaR,int mes,int mesR,int anho,int anhoR)
{   /** MODULO QUE CALCULA DIAS ENTRE DOS FECHAS **/
    long int cant_dias;
    int AUX, AUX2, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11;
    if((anho != anhoR) || (mes != mesR))
    {
        d1 = 31, d2 = 59, d3 = 90, d4 = 120, d5 = 151, d6 = 181, d7 = 212, d8 = 243, d9 = 273, d10 = 304, d11 = 334;
        switch(mes) /* dependiendo del mes se cuantos dias pasaron desde el 1 de enero en adelante */
        {
            case 1:
                AUX = dia;
            break;
            case 2:
                AUX = d1 + dia;
            break;
            case 3:
                AUX = d2 + dia;
            break;
            case 4:
                AUX = d3 + dia;
            break;
            case 5:
                AUX = d4 + dia;
            break;
            case 6:
                AUX = d5 + dia;
            break;
            case 7:
                AUX = d6 + dia;
            break;
            case 8:
                AUX = d7 + dia;
            break;
            case 9:
                AUX = d8 + dia;
            break;
            case 10:
                AUX = d9 + dia;
            break;
            case 11:
                AUX = d10 + dia;
            break;
            case 12:
                AUX = d11 + dia;
            break;
            }   /* Fin de switch */
        switch(mesR)
        {
            case 1:
                AUX2 = diaR;
            break;
            case 2:
                AUX2 = d1 + diaR;
            break;
            case 3:
                AUX2 = d2 + diaR;
            break;
            case 4:
                AUX2 = d3 + diaR;
            break;
            case 5:
                AUX2 = d4 + diaR;
            break;
            case 6:
                AUX2 = d5 + diaR;
            break;
            case 7:
                AUX2 = d6 + diaR;
            break;
            case 8:
                AUX2 = d7 + diaR;
            break;
            case 9:
                AUX2 = d8 + diaR;
            break;
            case 10:
                AUX2 = d9 + diaR;
            break;
            case 11:
                AUX2 = d10 + diaR;
            break;
            case 12:
                AUX2 = d11 + diaR;
            break;
        }   /* Fin de switch */
        if(anho == (anhoR - 1))
        {
            cant_dias = (365 - AUX) + AUX2;  /* aqui se tiene en cuenta si son 2 anhos contiguos */
        }
        else
        {
            cant_dias = ((365 - AUX) + AUX2) + (((anhoR - anho) - 1) * 365);  /* si no es el caso se suma diferencias */
        }
    }
    else
    {
        cant_dias = diaR - dia;   /* para 2 fechas del mismo mes y anho */
    }
    return cant_dias;
}
