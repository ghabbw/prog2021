/**
TP02-EJ06- Mejoramos el ejercicio 3 del TP de nivel 1
a) Generar un módulo para validar el tipo de vehículo. Para ello vamos ingresar el tipo de vehículo en una
variable de tipo char donde solo se puede ingresar la letra mayúscula M para motocicletas, A para
automóvil y C para camioneta. Este módulo solo va a retornar alguna de esas letras. Adaptar el
programa para que utilice dicho módulo.
b) Generar un módulo que calcule el importe a pagar por un vehículo y luego adecue el programa para
que utilice dicho modulo.
L___>>>>
    TP01-EJ03- Se desea construir un programa para ser utilizado en una playa de estacionamiento, el
    mismo comienza calculando el importe que debe pagar un automovilista por estacionar su vehículo en
    el estacionamiento (más adelante lo vamos a retomar). Las tarifas son:
    - Motocicleta: paga el importe correspondiente a la tarifa básica por cada hora de
        estacionamiento.
    - Automóvil: paga el doble de la tarifa básica por cada hora de estacionamiento.
    - Camioneta: paga el triple de la tarifa básica por cada hora de estacionamiento.
    Dado el tipo de vehículo, la hora de entrada y de salida indicar el importe a pagar.
        
    Nota: La fracción de hora se paga como hora entera. La tarifa básica es constante. La hora de
    entrada y salida corresponden a un mismo día.
**/

#include <stdio.h>
#define TARIFA_BASICA 50
#define AUTO TARIFA_BASICA * 2
#define CAMIONETA TARIFA_BASICA * 3
int calc_importe (int, int, int, int, int);
int valida_char (int, int, int, int, int);
int ingresa_hr (void);
int ingresa_mn (void);

int main(void)
{
    int importe, seleccion, validez, hrEntrada, mnEntrada, hrSalida, mnSalida;
    do
    {
        printf("\n TARIFA BASICA ACTUAL : %d\n", TARIFA_BASICA);
        printf("\n Ingrese la opcion:  ");
        printf("\n Tipo de vehiculo:  M ->moto    A ->auto    C ->camioneta    S -> Salir\n \t--->");
        do
        {
            printf("\n Ingrese su seleccion:   ");
            fflush(stdin);
            seleccion = getchar();
            if(seleccion >= 97 && seleccion <= 122)    /* pregunta si esta entre 'a' y 'z' */
            {   /* en caso verdadero lo convierte en su version mayuscula restando 32 segun tabla ASCII */
                seleccion = seleccion - 32;
            }
            validez = valida_char(seleccion, 'S', 'M', 'C', 'A');
            if(validez == 0)
            {
                printf("\n----Reintente... ingresos validos: 'S'  'M'  'C'  'A'");
            }
        }while(validez == 0);
        switch(seleccion)
        {
            case 'S':
                printf("\n Saliendo del programa...");
            break;
            case 'M':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, TARIFA_BASICA);
                printf("\n Total a pagar >>> $%d <<< \n\n----------------",importe);
            break;
            case 'A':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, AUTO);
                printf("\n Total a pagar >>> $ %d <<< \n\n----------------",importe);
            break;
            case 'C':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, CAMIONETA);
                printf("\n Total a pagar >>> $%d <<< \n\n----------------",importe);
            break;
        }
    }while(seleccion != 'S');
	
    return 0;
}
int valida_char (int entrada, int val_1, int val_2, int val_3, int val_4)
{
    if(entrada == val_1 || entrada == val_2 || entrada == val_3 || entrada == val_4)
    {   /* verifica que la entrada sea igual que uno de los valores pedidos para salir del bucle */
        return 1;
    }
    return 0;
}
int calc_importe (int hrEnt, int mnEnt, int hrSal, int mnSal, int tarifa)
{   /** este modulo no controla que la hora de salida sea mayor que la entrada; no fracciona **/
    int sal;
    sal = hrSal - hrEnt;
    if(mnSal > mnEnt)
    {
        sal = sal + 1;
    }
    return sal * tarifa;
}
int ingresa_hr (void)
{
    int hr, band;
    band = 0;
    printf("\n FORMATO DE HORA: 24HS...");
    do
    {
        if(band == 1)
        {
            printf("\n Hora fuera de rango... reintente...");
        }
        printf("\n Ingrese hora:  ");
        scanf("%d", &hr);
        band = 1;
    }while(hr < 0 || hr > 24);
    return hr;
}
int ingresa_mn (void)
{
    int mn, band;
    band = 0;
    do
    {
        if(band == 1)
        {
            printf("\n Minutos fuera de rango... reintente...");
        }
        printf("\n Ingrese minutos:  ");
        scanf("%d", &mn);
        band = 1;
    }while(mn < 0 || mn > 60);
    return mn;
}
