/**
TP02-EJ03- Dado un n�mero natural determine si es un n�mero deficiente, abundante o perfecto.
Nota: Un n�mero natural X se dice que es deficiente si la suma de sus divisores (sin contarse a s� mismo) es menor a X.
Si la suma de sus divisores es mayor a X se dice que el n�mero es abundante y si es igual a X se dice que es
perfecto.
**/
#include <stdio.h>
int suma_divs(int);

int main(void)
{
    int natural, sumatoria_dv, band;
    band = 0;
    do
    {
        printf("\n Ingrese numero natural para analizar:   ");
        scanf("%d", &natural);
        if(natural > 0)
        {
            band = 1;
        }
    }while(band == 0);
    sumatoria_dv = suma_divs(natural);
    if(sumatoria_dv <= natural)
    {
        if(sumatoria_dv == natural)
        {
            printf("\n Numero natural perfecto.");
        }
        else
        {
            printf("\n Numero natural deficiente.");
        }
    }
    else
    {
        printf("\n Numero natural abundante.");
    }
    
    return 0;
}
int suma_divs(int nat)
{
    int pd, acum;
    pd = 1, acum = 0;
    while(pd <= nat / 2)
    {
        if(nat % pd == 0)
        {
            acum = acum + pd;
        }
        pd++;
    }
    return acum;
}
