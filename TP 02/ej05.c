/**
TP02-EJ05- Realizar un programa que permita el ingreso de un número natural, y posteriormente calcule su
equivalente a base 2, si la cantidad de divisores del número es primo y calcule su equivalente a base 9 si la
cantidad de divisores no es primo.
**/
#include <stdio.h>
#include <math.h>
#define SI_PRIMO 2
#define NO_PRIMO 9

int convierte_base(int, int);
int potencia(int, int);
int cuenta_divs(int);
int det_primo(int);

int main(void)
{
    int natural, nueva_base, cant_divisores;
    
    printf("\n Ingrese numero natural:   ");
    scanf("%d", &natural);
    cant_divisores = cuenta_divs(natural);
    printf("\n cant div  %d", cant_divisores);
    if(det_primo(cant_divisores))
    {
        nueva_base = convierte_base(natural, SI_PRIMO);
        printf("\n Numero de divisores es primo -> convertido a binario: %d", nueva_base);
    }
    else
    {
        nueva_base = convierte_base(natural, NO_PRIMO);
        printf("\n Numero de divisores NO es primo -> convertido a base 9: %d", nueva_base);
    }
    
    return 0;
}
int potencia(int base, int exp)
{
    int i, salida = 1;
    for(i = 1; i <= exp; i++)
    {
        salida = salida * base;
    }
    return salida;
}
int convierte_base(int vieja, int nueva)
{
    int i, salida, dig;
    salida = 0, i = 0;
    while(vieja > 0)
    {
        dig = vieja % nueva;
        vieja = vieja / nueva;
        salida = salida + dig * potencia(10, i);
        i++;
    }
    return salida;
}
int cuenta_divs(int nat)
{
    int pd, cant_divs;
    pd = 1, cant_divs = 0;
    while(pd <= nat / 2)
    {
        if(nat % pd == 0)
        {
            cant_divs++;
        }
        pd++;
    }
    cant_divs++;    /** +1 un numero es divisible por si mismo **/
    return cant_divs;
}
int det_primo(int PP)
{   /* PP : Posible Primo */
    int pd = 2;    /* posible divisor */
    while((pd <= sqrt(PP)) && (PP % pd != 0))
    {
        pd++;
    }
    if((pd > sqrt(PP)) && (PP != 1))
    {
        return 1;   /** si es primo **/
    }
    return 0;   /** si NO es primo **/
}
