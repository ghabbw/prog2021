/** Gonzales Gabriel Antonio
TP02-EJ08-Realizar un programa que simule la evolucin de un set de un partido de tenis. Recuerde que un
set lo gana el jugador que primero llega a seis games (no se tendr en cuenta el tie-break). Para simular el
usuario se ingresara un nmero entero con los digitos 1 y 2, que indican cual jugador ha ganado cada punto de
un game (el numero representa los datos del game). Para cada digito del numero se indicara el nuevo marcador
del game. Se supondra que el game lo gana el jugador que consigue cuatro puntos.
Considerar que:
- El primer punto que consigue el jugador es quince Cuando el jugador consigue el segundo punto es treinta.
- Cuando el jugador consigue el tercer punto es cuarenta.
- Cuando el jugador consigue el cuarto punto, gana el juego. Por ejemplo: si S=112211, la salida sera
quince-nada, treinta-nada, treinta-quince, iguales treinta, juego para el jugador 1.
Si S= 1221211 sera quince-nada, iguales quince, quince-treinta, iguales-treinta, cuarenta, treinta,
iguales cuarenta, juego para el jugador 1
**/
#include <stdio.h>

unsigned long invierte_dato(unsigned long);
int potencia(int,int);
int main(void)
{
	unsigned long dato_set, invertido;
    int band, player1, player2, dig;
    band = 0;
    player1 = 0;
    player2 = 0;
    do
    {
        if(band)
        {
            printf("\n El dato es demasiado largo, 7 cifras como maximo...");
        }
        printf("\n Ingrese los datos del set: ");
        scanf("%lu", &dato_set);
        band = 1;
    }while(dato_set > 9999999);
    invertido = invierte_dato(dato_set);
    band = 0;
    while(invertido != 0)
    {
        dig = invertido % 10;
        invertido = invertido / 10;
        if(band == 0)
        {
            if(dig == 1)
            {
                player1++;
                if(player1 == 4)
                {
                    band = 1;
                }
                if(player1 != player2)
                {
                    if(player1 == 0 && player2 == 1)
                    {
                        printf("\nnada-quince");
                    }
                    if(player1 == 0 && player2 == 2)
                    {
                        printf("\nnada-treinta");
                    }
                    if(player1 == 0 && player2 == 3)
                    {
                        printf("\nnada-cuarenta");
                    }
                    if(player1 == 1 && player2 == 2)
                    {
                        printf("\nquince-treinta");
                    }
                    if(player1 == 1 && player2 == 3)
                    {
                        printf("\nquince-cuarenta");
                    }
                    if(player1 == 2 && player2 == 3)
                    {
                        printf("\ntreinta-cuarenta");
                    }
                    if(player2 == 4)
                    {
                        printf("\njuego para el jugador 2");
                    }
                    if(player1 == 1 && player2 == 0)
                    {
                        printf("\nquince-nada");
                    }
                    if(player1 == 2 && player2 == 0)
                    {
                        printf("\ntreinta-nada");
                    }
                    if(player1 == 3 && player2 == 0)
                    {
                        printf("\ncuarenta-nada");
                    }
                    if(player1 == 2 && player2 == 1)
                    {
                        printf("\ntreinta-quince");
                    }
                    if(player1 == 3 && player2 == 1)
                    {
                        printf("\ncuarenta-quince");
                    }
                    if(player1 == 3 && player2 == 2)
                    {
                        printf("\ncuarenta-treinta");
                    }
                    if(player1 == 4)
                    {
                        printf("\njuego para el jugador 1");
                    }
                }
                else
                {
                    printf("\nIguales ");
                    switch(player1)
                    {
                        case 1:
                            printf("quince");
                        break;
                        case 2:
                            printf("treinta");
                        break;
                        case 3:
                            printf("cuarenta");
                        break;
                    }
                }
            }
            if(dig == 2)
            {
                player2++;
                if(player2 == 4)
                {
                    band = 1;
                }
                if(player1 != player2)
                {
                    if(player1 == 0 && player2 == 1)
                    {
                        printf("\nnada-quince");
                    }
                    if(player1 == 0 && player2 == 2)
                    {
                        printf("\nnada-treinta");
                    }
                    if(player1 == 0 && player2 == 3)
                    {
                        printf("\nnada-cuarenta");
                    }
                    if(player1 == 1 && player2 == 2)
                    {
                        printf("\nquince-treinta");
                    }
                    if(player1 == 1 && player2 == 3)
                    {
                        printf("\nquince-cuarenta");
                    }
                    if(player1 == 2 && player2 == 3)
                    {
                        printf("\ntreinta-cuarenta");
                    }
                    if(player2 == 4)
                    {
                        printf("\njuego para el jugador 2");
                    }
                    if(player1 == 1 && player2 == 0)
                    {
                        printf("\nquince-nada");
                    }
                    if(player1 == 2 && player2 == 0)
                    {
                        printf("\ntreinta-nada");
                    }
                    if(player1 == 3 && player2 == 0)
                    {
                        printf("\ncuarenta-nada");
                    }
                    if(player1 == 2 && player2 == 1)
                    {
                        printf("\ntreinta-quince");
                    }
                    if(player1 == 3 && player2 == 1)
                    {
                        printf("\ncuarenta-quince");
                    }
                    if(player1 == 3 && player2 == 2)
                    {
                        printf("\ncuarenta-treinta");
                    }
                    if(player1 == 4)
                    {
                        printf("\njuego para el jugador 1");
                    }
                }
                else
                {
                    printf("\nIguales ");
                    switch(player1)
                    {
                        case 1:
                            printf("quince");
                        break;
                        case 2:
                            printf("treinta");
                        break;
                        case 3:
                            printf("cuarenta");
                        break;
                    }
                }
            }
        }
    }
    
	return 0;
}
int cuenta_dig(unsigned long numero)
{
    int cant_dig;
    cant_dig = 0;
    while(numero != 0)
    {
        cant_dig++;
        numero = numero / 10;
    }
    return cant_dig;
}
unsigned long invierte_dato(unsigned long dato)
{
    unsigned long inverso = 0;
    int cant_dig, dig;
    cant_dig = cuenta_dig(dato);
    while(dato != 0)
    {
        cant_dig--;
        dig = dato % 10;
        inverso = inverso + (dig * potencia(10, cant_dig));
        dato = dato / 10;
    }
    return inverso;
}
int potencia(int base, int exp)
{
    int i, salida = 1;
    for(i = 1; i <= exp; i++)
    {
        salida = salida * base;
    }
    return salida;
}
