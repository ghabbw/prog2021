/**
TP02-EJ01-Crear un programa que dados N numeros enteros, calcule la cantidad de d�gitos de cada numero.
Si la cantidad de digitos resulta ser un numero primo entonces crear un nuevo numero calculando la potencia
del numero ingresado, elevado al digito mas significativo. Si la cantidad de digitos no es primo, crear un nuevo
numero calculando la raiz cuadrada del numero. Mostrar cada numero generado indicando el tipo de
transformacion.
Nota: la funcion de potencia y de raiz cuadrada se encuentran en la librer�a <math.h>
**/
#include <stdio.h>
#include <math.h>

int cuenta_Dig(int);
int verifica_Primo(int);
int det_dig_sign(int, int);

int main(void)
{
    int i, N, cant_dig, entero;
    float nuevo_num;
    printf("\n Cantidad N de numeros enteros a ingresar\n");
    scanf("%d", &N);
    for(i = 1; i <= N; i++)
    {
        printf("\n --->");
        scanf("%d", &entero);
        cant_dig = cuenta_Dig(entero);
        if(verifica_Primo(cant_dig))
        {
            nuevo_num = pow(entero, det_dig_sign(entero, cant_dig));
            printf("\n Numero elevado al digito mas significado: %.2f", nuevo_num);
        }
        else
        {
            nuevo_num = sqrt(entero);
            printf("\n Raiz cuadrada del numero:  %.2f", nuevo_num);
        }
    }
    
    return 0;
}

int cuenta_Dig(int entero)
{
    int cont = 0;
    while(entero != 0)
    {
        entero = entero / 10;
        cont++;
    }
    return cont;
}
int verifica_Primo(int PP)
{   /* PP : Posible Primo */
    int pd = 2;    /* posible divisor */
    while((pd <= sqrt(PP)) && (PP % pd != 0))
    {
        pd++;
    }
    if((pd > sqrt(PP)) && (PP != 1))
    {
        return 1;
    }
    return 0;
}
int det_dig_sign(int entero, int cant_Dgt)
{
    int sign;
    sign = entero / pow(10, cant_Dgt - 1);
    printf("\n --->>>>>>>%d", sign);
    return sign;
}
