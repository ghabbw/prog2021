/**
TP04-EJ01- Dada una lista de N números enteros, se solicita:
a) Cargar la lista en un vector.
b) Luego de cargar la lista se pide ordenar la misma y que mediante un menú se pueda:
1. Dado un elemento por el usuario, indicar cuántas veces se encuentra.
2. Dado un elemento por el usuario, eliminarlo del vector todas las veces que se encuentre.
3. Dado un elemento por el usuario, agregarlo respetando el orden.
4. Mostrar todos los elementos del vector.
**/
#include <stdio.h>
#define tam_max 50

typedef int t_vector_int[tam_max];

void agrega_elemento_lis (t_vector_int, int *, int);
void carga_vector (t_vector_int, int *);
void muestra_vector (t_vector_int, int);
void ord_vector (t_vector_int, int);
void elim_rep_elem (t_vector_int, int *, int);
int cuenta_rep_elem (t_vector_int, int, int);
int menu (void);

int main (void)
{
    t_vector_int lista;
    int tam, opcion, elemn, cant_elem;
    
    carga_vector(lista, &tam);
    ord_vector(lista, tam);
    muestra_vector(lista, tam);
    do
    {
        opcion = menu();
        switch(opcion)
        {
            case 0:
                printf("\n Saliendo del programa...");
            break;
            case 1:
                printf("\n Ingrese elemento a cuantificar:  ");
                scanf("%d", &elemn);
                cant_elem = cuenta_rep_elem(lista, tam, elemn);
                printf("\n Cantidad :%d", cant_elem);
            break;
            case 2:
                printf("\n Ingrese elemento a eliminar:  ");
                scanf("%d", &elemn);
                elim_rep_elem(lista, &tam, elemn);
            break;
            case 3:
                printf("\n Ingrese elemento a agregar:  ");
                scanf("%d", &elemn);
                agrega_elemento_lis(lista, &tam, elemn);
            break;
            case 4:
                muestra_vector(lista, tam);
            break;
            default:
                printf("\n Opcion incorrecta...");
        }
    }while(opcion != 0);

    return 0;
}
int menu (void)
{
    int opcion;
    printf("\n\n 0- Salir");
    printf("\n 1- Ingresar elemento para contar las veces que se repite.");
    printf("\n 2- Ingresar elemento para eliminarlo del vector, incluyendo repeticiones.");
    printf("\n 3- Ingresar elemento para agregarlo sin perder el orden.");
    printf("\n 4- Mostrar todos los elementos del vector.");
    printf("\n\n Ingrese su opcion:\n       ");
    scanf("%d", &opcion);
    return opcion;
}
void agrega_elemento_lis (t_vector_int lista, int * tam, int elemn)
{   /** puede agregar aun cuando la lista esta vacia **/
    int i, band;
    band = 0;
    lista[0] = elemn;
    i = * tam;
    do
    {
        if(lista[i] > elemn)
        {
            lista[i + 1] = lista[i];
        }
        else
        {
            lista[i + 1] = elemn;
            band = 1;
        }
        i--;
    }while(band == 0);
    * tam = * tam + 1;
}
void carga_vector (t_vector_int lis, int * N)
{
    int i;

    printf("\n Ingrese cantidad de enteros:  ");
    scanf("%d", N);
    for(i = 1; i <= * N; i++)
    {
        printf("\n Ingrese entero:  ");
        scanf("%d", &lis[i]);
    }
}
void muestra_vector (t_vector_int lis, int tam)
{
    int i;
    for(i = 1; i <= tam; i++)
    {
        printf("\n %d", lis[i]);
    }
}
void ord_vector (t_vector_int lis, int tam)
{
    int i, j, aux;
    for(i = 1; i < tam; i++)
    {
        for(j = i + 1; j <= tam; j++)
        {
            if(lis[i] > lis[j])
            {
                aux = lis[i];
                lis[i] = lis[j];
                lis[j] = aux;
            }
        }
    }
}
int cuenta_rep_elem (t_vector_int lista, int tam, int elemn)
{
    int i, cont;
    cont = 0;
    for(i = 1; i <= tam; i++)
    {
        if(lista[i] == elemn)
        {
            cont++;
        }
    }
    return cont;
}
int busca_elemento (t_vector_int lista, int * tam, int elemn)
{   /** para vectores que comienzan en la posicion 1 **/
    int i;
    i = 1;
    while(i <= * tam && lista[i] != elemn)
    {
        i++;
    }
    if(i <= * tam)
    {
        return i;
    }
    return 0;
}
void elimina_uno (t_vector_int lista, int * tam, int pos)
{
    int i;
    for(i = pos; i <= * tam; i++)
    {
        lista[i] = lista[i + 1];
    }
    * tam = * tam - 1;
}
void elim_rep_elem (t_vector_int lista, int * tam, int elemn)
{   /** elimina en listas ord o desord **/
    int band, bus;
    bus = 0;
    do
    {
        bus = busca_elemento(lista, tam, elemn);
        if(bus != 0)
        {
            elimina_uno(lista, tam, bus);
            band = 1;
        }
    }while(bus != 0);
    if(band == 0)
    {
        printf("\n Elemento no encontrado...");
    }
    else
    {
        printf("\n Depuracion completa.");
    }
}
