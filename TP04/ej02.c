/**
TP04-EJ02- Un supermercado necesita actualizar los precios de N productos.
Para ello cuenta con 2 listas correspondientes donde el supermercado almacena los códigos de los N
productos y el precio actual de góndola. Estas listas se van cargando de forma ordenada por código.
Luego se reciben otras dos listas correspondientes que contienen los códigos de M productos y el
nuevo precio. Se debe actualizar sólo aquellos que sufrieron alguna modificación de precio. Se pide
además determinar cuál fue el porcentaje promedio de incremento, luego indicar cuántos productos
sufrieron un porcentaje de aumento mayor al promedio.
**/

#include <stdio.h>
#define Tam_Max 50

typedef float t_vec_prec[Tam_Max];
typedef long unsigned t_vec_cod[Tam_Max];

int cant_prom_incr (float, t_vec_cod, t_vec_prec, int, t_vec_cod, t_vec_prec, int);
float porcent_incr (t_vec_cod, t_vec_prec, int, t_vec_cod, t_vec_prec, int);
void ord_vectores (t_vec_cod, t_vec_prec , int);
void carga_vectores (t_vec_prec, t_vec_cod, int *);
void act_prec (t_vec_cod, t_vec_prec, int, t_vec_cod, t_vec_prec, int);
void muestra_vector_precios (t_vec_prec, int);
void muestra_vector_cods (t_vec_cod, int);

int main (void)
{
    t_vec_prec lis_precios_1, lis_precios_2;
    t_vec_cod lis_cods_1, lis_cods_2;
    int N, M, cant;
    float prom_porcent;
    
    printf("\n Cargando listas originales:\n");
    carga_vectores(lis_precios_1, lis_cods_1, &N);
    ord_vectores(lis_cods_1, lis_precios_1, N);
    printf("\n Mostrando las listas originales ordenadas:\n");
    muestra_vector_cods(lis_cods_1, N);
    muestra_vector_precios(lis_precios_1, N);

    printf("\n Cargando listas con precios actualizados:\n");
    carga_vectores(lis_precios_2, lis_cods_2, &M);
    printf("\n Mostrando las listas recien cargadas:\n");
    muestra_vector_cods(lis_cods_2, M);
    muestra_vector_precios(lis_precios_2, M);
    
    /** se obtienen los datos de cantidad de items con incremento y promedio antes de actualizar **/
    prom_porcent = porcent_incr(lis_cods_1, lis_precios_1, N, lis_cods_2, lis_precios_2, M);
    cant = cant_prom_incr(prom_porcent, lis_cods_1, lis_precios_1, N, lis_cods_2, lis_precios_2, M);
    act_prec(lis_cods_1, lis_precios_1, N, lis_cods_2, lis_precios_2, M);
    printf("\n\n Mostrando las listas originales actualizadas\n");
    muestra_vector_cods(lis_cods_1, N);
    muestra_vector_precios(lis_precios_1, N);
    printf("\n Porcentaje promedio de incremento en los precios: %.2f", prom_porcent);
    printf("\n Cant. de incrementos sobre el promedio porcentual: %d", cant);
    return 0;
}
int busc_cod (t_vec_cod lis_cods, int tam, long unsigned cod_bus)
{
    int ini, fin, med;
    ini = 1, fin = tam;
    med = (ini + fin) / 2;
    while((ini <= fin) && (lis_cods[med] != cod_bus))
    {
        if(lis_cods[med] < cod_bus)
        {
            ini = med + 1;
        }
        else
        {
            fin = med - 1;
        }
        med = (ini + fin) / 2;
    }
    if(ini <= fin)
    {
        return med;
    }
    return -1;
}
int cant_prom_incr (float prom, t_vec_cod c01, t_vec_prec p01, int N, t_vec_cod c02, t_vec_prec p02, int M)
{
    int i, pos, cant;
    float porcent_incr;
    cant = 0;
    for(i = 1; i <= M; i++)
    {
        pos = busc_cod(c01, N, c02[i]);
        if(pos != -1)
        {
            if(p01[pos] < p02[i])
            {   /** el nuevo precio es mayor al original **/
                porcent_incr = ((p02[i] - p01[pos]) * 100) / p01[pos];
                if(porcent_incr > prom)
                {
                    cant++;
                }
            }
        }
    }
    return cant;
}
float porcent_incr (t_vec_cod c01, t_vec_prec p01, int N, t_vec_cod c02, t_vec_prec p02, int M)
{
    int i, pos, cant;
    float acum;
    cant = 0, acum = 0;
    for(i = 1; i <= M; i++)
    {
        pos = busc_cod(c01, N, c02[i]);
        if(pos != -1)
        {
            if(p01[pos] < p02[i])
            {   /** el nuevo precio es mayor al original **/
                acum = acum + ((p02[i] - p01[pos]) * 100) / p01[pos];
                cant++;
            }
        }
    }
    if(cant > 0)
    {
        return acum / cant;
    }
    return 0;
}
void act_prec (t_vec_cod c01, t_vec_prec p01, int N, t_vec_cod c02, t_vec_prec p02, int M)
{
    int i, pos;
    for(i = 1; i <= M; i++)
    {
        pos = busc_cod(c01, N, c02[i]);
        if(pos != -1)
        {
            if(p01[pos] != p02[i])
            {
                p01[pos] = p02[i];
            }
        }
    }
}
void carga_vector_cods (t_vec_cod lis, int N)
{
    int i;
    printf("\n Ingrese codigos");
    for(i = 1; i <= N; i++)
    {
        printf("\n  < %d / %d >    ", i, N);
        scanf("%lu", &lis[i]);
    }
}
void carga_vector_precios (t_vec_prec lis, int N)
{
    int i;
    printf("\n Ingrese precios");
    for(i = 1; i <= N; i++)
    {
        printf("\n  < %d / %d >    ", i, N);
        scanf("%f", &lis[i]);
    }
}
void carga_vectores (t_vec_prec lis_precios, t_vec_cod lis_cods, int * N)
{
    printf("\n Ingrese cantidad de productos:  ");
    scanf("%d", N);
    carga_vector_cods(lis_cods, * N);
    carga_vector_precios(lis_precios, * N);
}
void muestra_vector_cods (t_vec_cod lis_cods, int tam)
{
    int i;
    for(i = 1; i <= tam; i++)
    {
        printf("\n %lu", lis_cods[i]);
    }
}
void muestra_vector_precios (t_vec_prec lis_precios, int tam)
{
    int i;
    for(i = 1; i <= tam; i++)
    {
        printf("\n %.2f", lis_precios[i]);
    }
}
void ord_vectores (t_vec_cod lis_cods, t_vec_prec lis_precios, int tam)
{
    int i, j;
    long unsigned aux_lu;
    float aux_f;
    for(i = 1; i < tam; i++)
    {
        for(j = i + 1; j <= tam; j++)
        {
            if(lis_cods[i] > lis_cods[j])
            {
                aux_lu = lis_cods[i];
                lis_cods[i] = lis_cods[j];
                lis_cods[j] = aux_lu;
                
                aux_f = lis_precios[i];
                lis_precios[i] = lis_precios[j];
                lis_precios[j] = aux_f;
            }
        }
    }
}
