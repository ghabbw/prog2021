/**
TP01-EJ05-Dada una lista de n�meros enteros, distintos de cero, se desea obtener el promedio de los
n�meros que est�n formados �nicamente con d�gitos pares.
**/
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int entero, acum, auxiliar, digito, band, cont;
    float promedio;
    band = 0, acum = 0, cont = 0;
    do
    {
        printf("\n Ingrese un numero entero                       Cero para terminar ingreso.\n    ");
        scanf("%d", &entero);
        auxiliar = abs(entero);
        while(auxiliar != 0 && band == 0)
        {
            digito = auxiliar % 10;
            auxiliar = auxiliar / 10;
            if(digito % 2 != 0)
            {
                band = 1;
            }
        }
        if(!band && entero != 0)
        {
            acum = acum + entero;
            cont++;
        }
        band = 0;
    }while(entero != 0);
    if(cont)
    {
        promedio = acum / (float)cont;
        printf("\n El promedio de los enteros formados solo por digitos pares es: %.2f", promedio);
    }
    else
    {
        printf("\n Ningun entero cumplia las condiciones.");
    }
    
    return 0;
}
