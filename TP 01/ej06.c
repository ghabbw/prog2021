/**
TP01-EJ06-Dado un n�mero natural K de dos cifras, se pide mostrar los n�meros naturales primos
que le anteceden. Por ejemplo, si K= 12 la salida ser� {2, 3, 5, 7, 11).
Nota: Debe validar el valor de K. Esto significa que el programa no debe avanzar mientras el valor de
K no cumpla con la condici�n.
**/
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int k, posible_primo, posible_divisor;
    
    do
    {
        printf(" Ingrese K:                    Debe ser de 2 cifras.\n");
        scanf("%d", &k);
    }while(k < 10); /* guardian que obliga que k sea de 2 digitos o vuelve a pedir k */
    printf(" Primos que anteceden a %d : 2, 3, 5, 7, ", k); /* son los primos < 10 que siempre se mostraran */
    posible_primo = 11; /* comienzo en 11 porque es impar, luego va de impar en impar, porque los primos > 10 son impares */
    while(posible_primo <= k)
    {
        posible_divisor = 2;
        while((posible_primo % posible_divisor != 0) && (posible_divisor <= posible_primo / 2))
        {
            posible_divisor++;
        }
        if(posible_divisor > posible_primo / 2)
        {
            printf("%d, ", posible_primo);
        }
        posible_primo = posible_primo + 2;
    }
    
    return 0;
}
