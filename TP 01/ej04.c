/**
TP01-EJ04-Dada una lista con n�meros naturales que representan las edades de N deportistas, se
necesita informar la edad promedio de los deportistas de la lista; cu�l es la menor y mayor edad
ingresada.
**/
#include <stdio.h>

int main(void)
{
    int edd, may, men, cantDep, sumEdd, i;
    float prom;
    sumEdd = 0, may = 0, men = 100; /* edad mayor = 0 y menor = 100 listos para ser reemplazados al primer ingreso */
    printf(" \n Ingrese la cantidad de deportistas: ");
    scanf("%d", &cantDep);
    for(i = 1; i <= cantDep; i++)
    {
        printf("\n Ingrese edad del deportista (%d / %d) : ", i, cantDep);
        scanf("%d", &edd);
        if(edd < men)   /* se actualiza el menor */
        {
            men = edd;
        }
        if(edd > may)   /* se actualiza el mayor */
        {
            may = edd;
        }
        sumEdd = sumEdd + edd;
    }
    if(cantDep != 0)    /* se cuida que no hay una division por cero */
    {
        prom = sumEdd / (float)cantDep; /* casting para evitar una division entera */
        printf("\n\n  Promedio de edad: %.2f", prom);
        printf("\n  Edad mayor: %d   ------ Edad Menor: %d", may, men);
    }
    else
    {
        printf("\n No hubo ingreso de datos.");
    }
    return 0;
}
