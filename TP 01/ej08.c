/*
TP01-EJ08-Dada una lista de números enteros, determinar la cantidad de sublistas ascendentes que
se encuentran en ella. No se debe considerar un solo elemento como sublista. No se deben contar
sublistas superpuestas.
Ejemplo: 75674567. Tiene 2 (dos) sublistas 567 y 4567
*/

#include <stdio.h>

int main(void)
{
    char continuar;
    int band, cont, ultimo, band_segundo, entero_A, entero_Z;
    
    band = 0, cont = 0, band_segundo = 0;
    printf("----se contarán las sublistas ascendentes de los números enteros que ingres----\n");
    printf("Ingrese un entero:  ");
    scanf("%d", &entero_A);
    printf("Ingrese -s- para continuar...\n\t");
    fflush(stdin);
    scanf("%c", &continuar);
    while(continuar == 's' || continuar == 'S')
    {
        band_segundo = 1;
        printf("Ingrese un entero:  ");
        scanf("%d", &entero_Z);
        if(entero_Z > entero_A) /* condicion que marca ascendencia */
        {
            band = 1;
        }
        else
        {
            if(band == 1)   /* si venia ascendente aumenta contador y resetea bandera */
            {
                cont++;
                band = 0;
            }
        }
        ultimo = entero_A;
        entero_A = entero_Z;    /* el ultimo numero agregado es guardado para comparar con el siguiente */
        printf("\n Si desea continuar presione -s-\n\t");
        fflush(stdin);
        scanf("%c", &continuar);
    }
    if(band_segundo == 0)   /* si entró un solo número */
    {
        printf("\n --- Ingresó un solo número, no hay sublistas ---");
    }
    else
    {
        if(entero_Z > ultimo)   /* para revisar el último número ingresado comparando con el penultimo */
        {
            cont++;
        }
        printf("\n Hay >> %d << sublistas ascendentes.", cont);
    }
	return 0;
}

