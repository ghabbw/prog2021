/** 
TP01-EJ03-Se desea construir un programa para ser utilizado en una playa de estacionamiento, el
mismo comienza calculando el importe que debe pagar un automovilista por estacionar su vehículo en
el estacionamiento (más adelante lo vamos a retomar). Las tarifas son:
- Motocicleta: paga el importe correspondiente a la tarifa básica por cada hora de
estacionamiento.
- Automóvil: paga el doble de la tarifa básica por cada hora de estacionamiento.
- Camioneta: paga el triple de la tarifa básica por cada hora de estacionamiento.
Dado el tipo de vehículo, la hora de entrada y de salida indicar el importe a pagar.
Nota: La fracción de hora se paga como hora entera. La tarifa básica es constante. La hora de
entrada y salida corresponden a un mismo día.
**/

#include <stdio.h>
#define TARIFA_BASICA 50
#define AUTO TARIFA_BASICA * 2
#define CAMIONETA TARIFA_BASICA * 3

int main(void)
{
    int hrEntrada, hrSalida, importe, tipo;
    do
    {
        printf("\n TARIFA ACTUAL : %d\n", TARIFA_BASICA);
        printf("\n Ingrese la opcion:  ");
        printf("\n Cero para salir");
        printf("\n Tipo de vehiculo:  1->moto    2->auto    3->camioneta    ");
        scanf("%d",& tipo);
        switch(tipo)
        {
            case 0:
                printf("\n Saliendo del programa...");
            break;
            case 1:
                printf("\n Ingrese hora de entrada (24hs):  ");
                scanf("%d",&hrEntrada);
                printf("\n Ingrese hora de salida (24hs):  ");
                scanf("%d",&hrSalida);
                importe = (hrSalida - hrEntrada) * TARIFA_BASICA;
                printf("\n Total a pagar %d \n\n----------------",importe);
            break;
            case 2:
                printf("\n Ingrese hora de entrada (24hs):  ");
                scanf("%d",&hrEntrada);
                printf("\n Ingrese hora de salida (24hs):  ");
                scanf("%d",&hrSalida);
                importe = (hrSalida - hrEntrada) * AUTO;
                printf("\n Total a pagar %d \n\n----------------",importe);
            break;
            case 3:
                printf("\n Ingrese hora de entrada (24hs):  ");
                scanf("%d",&hrEntrada);
                printf("\n Ingrese hora de salida (24hs):  ");
                scanf("%d",&hrSalida);
                importe = (hrSalida - hrEntrada) * CAMIONETA;
                printf("\n Total a pagar %d \n\n----------------",importe);
            break;
            
            default : printf("\n Tipo incorrecto");
        }
    }while(tipo != 0);
	
    return 0;
}
