/*
TP01-E07-Dados N caracteres que representan un párrafo, conformados solamente por letras y
espacios en blanco. Se pide informar la cantidad de vocales, la cantidad de consonantes y la cantidad
de palabras que contiene.
Nota: Entre las letras del párrafo no hay vocales acentuadas y las palabras se separan por un solo
espacio en blanco. Considerar que el ingreso se realiza de a un carácter por vez.
*/

#include <stdio.h>

int main(void)
{
    char ch;
    int i, n, cons, voc, pal, cant_char, cant_esp, primer_char, band_primero;
    
    pal = 0, cons = 0, voc = 0, cant_char = 0, primer_char = 0, cant_esp = 0, band_primero = 0;

    printf(" Ingrese la cantidad de caracteres (incluidos los espacios):  ");
    scanf("%d", &n);
	printf(" - - - - - - - - - letra por letra - - - - - - \n");
	
    for(i = 1; i <= n; i++)
    {
        fflush(stdin);
        printf("\n\t-------->");
        ch = getchar();
        if(band_primero == 0)
        {
            primer_char = ch;
            band_primero = 1;
        }

        if(ch != 32)    /* se muestra el ultimo caracter ingresado */
        {
            printf("Caracter ingresado: %c  progreso: <%d / %d> \n", ch, i, n);
        }
        else
        {
            printf("    <<>>> Ingresó un espacio. progreso: <%d / %d> \n", i, n);
        }

        if(ch >= 65 && ch <= 90)    /* pregunta si esta entre 'A' y 'Z' */
        {
            ch = ch + 32;   /* en caso verdadero lo convierte en su version minuscula sumando 32 segun tabla ASCII */
        }

        if(ch != ' ')
        {
            if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
            {
                voc++;
            }
            else    /* se cuentan vocales y consonantes */
            {
                cons++;
            }
            cant_char++;
        }
        else
        {
            cant_esp++;
        }
    }
    if(cant_char == n)  /* cuando no entra ningun espacio para contar las palabras */
    {
        pal = 1;
    }
    else
    {
        pal = cant_esp; /* cantidad estimada de palabras segun espacios */
        if(primer_char == ' ' && ch == ' ') /* el auxiliar primer_char guarda si lo primero y ultimo ingresado fue espacio */
        {
            pal--;
        }
        if(primer_char != ' ' && ch != ' ') /* si lo primero y ultimo ingresado era distinto del espacio */
        {
            pal++;
        }
    }
    printf("\n------------------------------");
    printf("\n\nCantidad de consonantes: %d", cons);
    printf("\n\nCantidad de vocales: %d", voc);
    printf("\n\nCantidad de palabras: %d", pal);

    return 0;
}
