/**
TP01-EJ01- Dado el radio de un circulo mostrar su perimetro y su superficie.
**/
#include <stdio.h>
#define PI 3.141592
int main(void)
{
    float radio, perimetro, superficie;
    
    printf("\n Ingrese radio: ");
    scanf("%f", &radio);
    printf("\n---------------------------------\n");
    perimetro = 2 * PI * radio;
    printf("\n El permetro es: %.2f", perimetro);
    superficie = PI * radio * radio;
    printf("\n La superficie es: %.2f", superficie);
    
    return 0;
}

