/**
TP01-EJ02- Calcular y mostrar la cantidad de horas, minutos y segundos que existen en una cantidad de segundos ingresada por el usuario.
**/
#include <stdio.h>

int main(void)
{
    long int seg;
    
    printf(" Ingrese la cantidad de segundo:  ");
    scanf("%ld", &seg);
    printf("\n Cantidad de horas:  %ld", seg / 3600);
    seg = seg % 3600;
    printf("\n Cantidad de minutos:  %ld", seg / 60);
    seg = seg % 60;
    printf("\n Cantidad de segundos:  %ld", seg);
    
    return 0;
}

