#include <stdio.h>
int cuenta_dig(unsigned long);
int potencia(int , int);
int main(void)
{
    long int entrada, salida;
    int opcion, band;
    band = 0;
    do
    {
        opcion = menu();
        scanf("%d", &opcion);
        switch(opcion)
        {
            case 0:
                printf("\n... Saliendo del programa...\n");
            break;
            case 1:
                do
                {   /** verifica que sea binario **/
                    printf("\nIngrese codigo binario:   ---->");
                    scanf("%li", &entrada);
                    if(err_entrada(entrada))
                    {
                        printf("\n El codigo binario esta corrupto... reintente...");
                        band = 1;
                    }
                    else
                    {
                        printf("\n El codigo binario es valido...");
                        band = 0;
                    }
                }while(band);
                salida = codifica_numero(entrada);
                printf("\n El numero codificado es:  %li", salida);
                printf("\n------------------------------------");
            break;
            case 2:
                printf("\nIngrese numero codificado:   >>>>");
                scanf("%li", &entrada);
                salida = decodifica_num(entrada);
                printf("\n El codigo binario es:  %li", salida);
                printf("\n------------------------------------");
            break;
            default:    printf("\n Opcion incorrecta...");
        }
    }while(opcion != 0);
	return 0;
}
int cuenta_dig(unsigned long numero)
{
    int cant_dig;
    cant_dig = 0;
    while(numero != 0)
    {
        cant_dig++;
        numero = numero / 10;
    }
    return cant_dig;
}
int potencia(int base, int exp)
{
    int i, salida = 1;
    for(i = 1; i <= exp; i++)
    {
        salida = salida * base;
    }
    return salida;
}
void menu()
{
    int opcion;
    printf("\n 1- Para ingresar binario");
    printf("\n 2- Para ingresar codificado");
    printf("\n Para salir 0");
    printf("\n Ingrese opcion:    >>>");
    scanf("\n%d", &opcion);
}
