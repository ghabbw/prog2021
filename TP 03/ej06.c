/**
TP03-EJ06- Dada una lista de N fechas ingresadas por el usuario como numeros enteros con el
formato (DDMMAAAA), se pide validar cada una de esas fechas e indicar cuantas fechas son validas
y cuantas no lo fueron. Se debe tener en cuenta los a�os bisiestos.
    Nota: un anho sera bisiesto si es divisible entre 4. Sin embargo, no puede ser divisible entre 100, a
    menos que tambien lo sea por 400. Se debe crear un modulo que separe la fecha en d�a, mes y a�o.
**/
#include <stdio.h>
#define tam_max 50

typedef char t_cadena[tam_max];

long unsigned carga_una_fecha(void);
int valida_fecha(int, int, int);
void separa_fecha (int *, int *, int *, long unsigned);

int main (void)
{
    int N, i, dia, mes, anho, val, cant_val;
    long unsigned fecha;
    cant_val = 0;
    printf("\n Ingrese cantidad de fechas a procesar:           <DDMMAAAA> \n  > > > > ");
    scanf("%d", &N);
    for(i = 1; i <= N; i++)
    {
        fecha = carga_una_fecha();
        separa_fecha(&dia, &mes, &anho, fecha);
        val = valida_fecha(dia, mes, anho);
        if(val == 1)
        {
            cant_val++;
        }
    }
    printf("\n Cantidad de fechas validas: %d", cant_val);
    printf("\n Cantidad de fechas NO validas: %d", N - cant_val);
    
    return 0;
}
long unsigned carga_una_fecha (void)
{
    long unsigned fecha;
    printf("\n Ingrese fecha: ");
    scanf("%lu", &fecha);
    return fecha;
}
void separa_fecha (int * dia, int * mes, int * anho, long unsigned fecha)
{
    * anho = fecha % 10000;
    fecha = fecha / 10000;
    * mes = fecha % 100;
    fecha = fecha / 100;
    * dia = fecha;
}
int det_bisiesto(int anho)
{   /** devuelve 1 si es bisiesto **/
    if(anho % 4 == 0 && (anho % 100 != 0 || anho % 400 == 0))
    {
        return 1;
    }
    return 0;
}
int valida_fecha(int dia, int mes, int anho)
{   /** devuelve 1 si es valido **/
    int band, limite;
    band = 1;
    
    switch(mes)
    {   /** dependiendo el mes... el limite tope de dias posibles para ese mes **/
        case 2:
            limite = 28;
        break;
        case 4: case 6: case 9: case 11:
            limite = 30;
        break;
        default:
            limite = 31;
    }
    if(dia < 1 || dia > limite)
    {
        band = 0;
    }
    if(det_bisiesto(anho) == 0)
    {   /** si no es bisiesto y tiene 29 de feb -> el modulo retorna 0 **/
        if(mes == 2 && dia == 29)
        {
            band = 0;
        }
    }
    else
    {
        if(mes == 2 && dia == 29)
        {   /** rectifica si el anho era bisiesto y band quedo igual a 0 en el paso anterior **/
            band = 1;
        }
    }
    if(mes < 1 || mes > 12)
    {   /** que el mes no sea cero ni mayor a 12 **/
        band = 0;
    }
    return band;
}
void lee_cad(t_cadena Cad, int tam) /** mod global **/
{
    int j, m;
    j = 0;
    while(j < tam && (m = getchar()) != EOF && m != '\n')
    {
        Cad[j] = m;
        j++;
    }
    Cad[j] = '\0';
    if(m != EOF && m != '\n')   /*limpia el buffer*/
    {
        while((m = getchar()) != EOF && m != '\n');
    }
}
