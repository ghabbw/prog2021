/**
TP03-EJ05- Mejoramos el ejercicio 6 del TP de nivel 2
Modificar el programa del estacionamiento de acuerdo a los siguientes requerimientos:
a) Se debe poder ingresar la fecha del dia y el saldo con el que se abre la caja. Esta tarea debe ser
    un modulo llamado abre_caja
b) Se debe poder ingresar una cantidad NO conocida de vehiculos y para cada vehiculo mostrar cuanto 
    debe abonar por su estadia
c) Al finalizar el dia debe mostrar la fecha, cantidad de vehiculos que ingresaron y al lado el importe
    recaudado, tambien mostrar el saldo total de dinero en la caja. Esta tarea debe ser un modulo 
    llamado cierre_dia
    L___>>>>>>
        TP02-EJ06- Mejoramos el ejercicio 3 del TP de nivel 1
        a) Generar un modulo para validar el tipo de vehículo. Para ello vamos ingresar el tipo de vehiculo en una
        variable de tipo char donde solo se puede ingresar la letra mayuscula M para motocicletas, A para
        automovil y C para camioneta. Este modulo solo va a retornar alguna de esas letras. Adaptar el
        programa para que utilice dicho modulo.
        b) Generar un modulo que calcule el importe a pagar por un vehículo y luego adecue el programa para
        que utilice dicho modulo.
        L___>>>>>>
            TP01-EJ03- Se desea construir un programa para ser utilizado en una playa de estacionamiento, el
            mismo comienza calculando el importe que debe pagar un automovilista por estacionar su vehiculo en
            el estacionamiento (mas adelante lo vamos a retomar). Las tarifas son:
            - Motocicleta: paga el importe correspondiente a la tarifa basica por cada hora de
                estacionamiento.
            - Automovil: paga el doble de la tarifa basica por cada hora de estacionamiento.
            - Camioneta: paga el triple de la tarifa basica por cada hora de estacionamiento.
            Dado el tipo de vehiculo, la hora de entrada y de salida indicar el importe a pagar.
                
            Nota: La fraccion de hora se paga como hora entera. La tarifa basica es constante. La hora de
            entrada y salida corresponden a un mismo dia.
**/
#include <stdio.h>
#define TARIFA_BASICA 50
#define AUTO TARIFA_BASICA * 2
#define CAMIONETA TARIFA_BASICA * 3
int calc_importe (int, int, int, int, int);
int valida_char (char, char, char, char, char);
int ingresa_hr (void);
int ingresa_mn (void);
int menu (void);
void convierte_mayus (char *);
void abre_caja (int *, int *, int *, float *);
void cierre_dia (int, int, int, float, int, float);

int main (void)
{
    int importe, validez, hrEntrada, mnEntrada, hrSalida, mnSalida, dia, mes, anho, cant_vehi;
    char seleccion;
    float saldo, recaudacion_dia;
    
    cant_vehi = 0;
    recaudacion_dia = 0;
    
    abre_caja(&dia, &mes, &anho, &saldo);
    do{
        printf("\n TARIFA BASICA ACTUAL : $ %d\n", TARIFA_BASICA);
        printf("\n Ingrese la opcion:  ");
        printf("\n Tipo de vehiculo:  M -> moto    A -> auto    C -> camioneta    S -> Salir\n \t--->");
        do
        {
            printf("\n Ingrese su seleccion:   ");
            fflush(stdin);
            seleccion = getchar();
            convierte_mayus(&seleccion);
            validez = valida_char(seleccion, 'S', 'M', 'C', 'A');
            if(validez == 0)
            {
                printf("\n----Reintente... ingresos validos: 'S'  'M'  'C'  'A'");
            }
        }while(validez == 0);
        switch(seleccion)
        {
            case 'S':
                printf("\n Saliendo del programa...");
            break;
            case 'M':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, TARIFA_BASICA);
                printf("\n Total a pagar >>> $%d <<< \n\n----------------",importe);
                recaudacion_dia = recaudacion_dia + importe;
                cant_vehi++;
            break;
            case 'A':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, AUTO);
                printf("\n Total a pagar >>> $ %d <<< \n\n----------------",importe);
                recaudacion_dia = recaudacion_dia + importe;
                cant_vehi++;
            break;
            case 'C':
                printf("\n Horario de Entrada .... >>>");
                hrEntrada = ingresa_hr();
                mnEntrada = ingresa_mn();
                printf("\n Horario de Salida .... >>>");
                hrSalida = ingresa_hr();
                mnSalida = ingresa_mn();
                importe = calc_importe(hrEntrada, mnEntrada, hrSalida, mnSalida, CAMIONETA);
                printf("\n Total a pagar >>> $%d <<< \n\n----------------",importe);
                recaudacion_dia = recaudacion_dia + importe;
                cant_vehi++;
            break;
        }
    }while(seleccion != 'S');
    saldo = recaudacion_dia + saldo;
    cierre_dia(dia, mes, anho, recaudacion_dia, cant_vehi, saldo);
    
    return 0;
}
void convierte_mayus (char * seleccion)
{
    if(* seleccion >= 97 && * seleccion <= 122)    /* pregunta si esta entre 'a' y 'z' */
    {   /* en caso verdadero lo convierte en su version mayuscula restando 32 segun tabla ASCII */
        * seleccion = * seleccion - 32;
    }
}
int valida_char (char entrada, char val_1, char val_2, char val_3, char val_4)
{
    if(entrada == val_1 || entrada == val_2 || entrada == val_3 || entrada == val_4)
    {   /* verifica que la entrada sea igual que uno de los valores pedidos para salir del bucle */
        return 1;
    }
    return 0;
}
int calc_importe (int hrEnt, int mnEnt, int hrSal, int mnSal, int tarifa)
{   /** este modulo no controla que la hora de salida sea mayor que la entrada; no fracciona **/
    int sal;
    sal = hrSal - hrEnt;
    if(mnSal > mnEnt)
    {
        sal = sal + 1;
    }
    return sal * tarifa;
}
void carga_una_fecha (int * dia, int * mes, int * anho)
{
    int band;
    band = 0;
    do
    {
        if(band == 1)
        {
            printf("\n Dia fuera de rango  DD ... reintente...");
        }
        printf("\n Ingrese dia: ");
        scanf("%d", dia);
        band = 1;
    }while(* dia < 1 || * dia > 31);
    band = 0;
    do
    {
        if(band == 1)
        {
            printf("\n Mes fuera de rango  MM ... reintente...");
        }
        printf("\n Ingrese mes: ");
        scanf("%d", mes);
        band = 1;
    }while(* mes < 1 || * mes > 12);
    band = 0;
    do
    {
        if(band == 1)
        {
            printf("\n Anho fuera de rango  AAAA ... reintente...");
        }
        printf("\n Ingrese anho: ");
        scanf("%d", anho);
        band = 1;
    }while(* anho < 2000 || * anho > 3000);
}
void abre_caja (int * dia, int * mes, int * anho, float * saldo)
{
    printf("\n Ingrese fecha de hoy:\n");
    carga_una_fecha(dia, mes, anho);
    printf("\n Ingrese el saldo:  $ ");
    scanf("%f", saldo);
}
void muestra_una_fecha (int dia, int mes, int anho)
{
    printf("\n Fecha: %d / %d / %d      (dd/mm/aaaa)", dia, mes, anho);
}
void cierre_dia (int dia, int mes, int anho, float recau_dia, int cant_vehi, float recau_total)
{
    muestra_una_fecha(dia, mes, anho);
    printf("\n Cantidad de vehiculos de la fecha: %d", cant_vehi);
    printf("\n Recaudacion de la fecha: %.3f", recau_dia);
    printf("\n Recaudacion total: %.3f", recau_total);
}
int ingresa_hr (void)
{
    int hr, band;
    band = 0;
    printf("\n FORMATO DE HORA: 24HS...");
    do
    {
        if(band == 1)
        {
            printf("\n Hora fuera de rango... reintente...");
        }
        printf("\n Ingrese hora:  ");
        scanf("%d", &hr);
        band = 1;
    }while(hr < 0 || hr > 24);
    return hr;
}
int ingresa_mn (void)
{
    int mn, band;
    band = 0;
    do
    {
        if(band == 1)
        {
            printf("\n Minutos fuera de rango... reintente...");
        }
        printf("\n Ingrese minutos:  ");
        scanf("%d", &mn);
        band = 1;
    }while(mn < 0 || mn > 60);
    return mn;
}
