/**
TP03-EJ07- Una agencia espacial transmite y recibe mensajes con sus sat�lites mediante una serie
de bits (0 � 1), Como estos mensajes son n�meros grandes, requieren ser codificados para que
viajen por el espacio. La codificaci�n consiste en reemplazar grupos de bits de un mismo valor por la
cantidad de bits del grupo, seguida del valor del bit, Por ejemplo, si el n�mero es = (11000011111) su
codificaci�n resulta: (214051). Cuando la agencia recibe un mensaje codificado hace el proceso
inverso para descubrir el mensaje original.
Se pide crear un programa para que, mediante un men� de opciones, la agencia pueda enviar o
recibir mensajes. Cuando el mensaje se env�a, el usuario ingresa dicho mensaje y se tiene que
modificar por la codificaci�n correspondiente. Y cuando el mensaje se recibe el usuario ingresa el
mensaje recibido y �ste se modifica por el mensaje original.
    Nota: Asumir que los n�meros binarios a codificar siempre empiezan con al menos un bit 1. Utilizar
    como tipo de dato long int para el n�mero binario y su codificaci�n. Hacer pruebas con n�meros no
    muy grandes pues de otra manera la memoria se desbordar�. M�s adelante se podr�n utilizar otros
    tipos de datos que permitan una mejor soluci�n al problema. Tambi�n tener en cuenta que se debe
    validar que el mensaje binario a enviar sea efectivamente un n�mero con solo unos y ceros.
**/
#include <stdio.h>

long int codifica_numero(long int);
long int decodifica_num(long int);
int err_entrada(long int);
int potencia(int, int);
int menu(void);

int main(void)  
{
    long int entrada, salida;
    int opcion, band;
    band = 0;
    do
    {
        opcion = menu();
        switch(opcion)
        {
            case 0:
                printf("\n... Saliendo del programa...\n");
            break;
            case 1:
                do
                {   /** verifica que sea binario **/
                    printf("\nIngrese codigo binario:   ---->");
                    scanf("%li", &entrada);
                    if(err_entrada(entrada))
                    {
                        printf("\n El codigo binario esta corrupto... reintente...");
                        band = 1;
                    }
                    else
                    {
                        printf("\n El codigo binario es valido...");
                        band = 0;
                    }
                }while(band);
                salida = codifica_numero(entrada);
                printf("\n El numero codificado es:  %li", salida);
                printf("\n------------------------------------");
            break;
            case 2:
                printf("\nIngrese numero codificado:   >>>>");
                scanf("%li", &entrada);
                salida = decodifica_num(entrada);
                printf("\n El codigo binario es:  %li", salida);
                printf("\n------------------------------------");
            break;
            default:    printf("\n Opcion incorrecta...");
        }
    }while(opcion != 0);
    return 0;
}
int potencia(int base, int exp)
{
    int i, salida = 1;
    for(i = 1; i <= exp; i++)
    {
        salida = salida * base;
    }
    return salida;
}
long int desenrrolla_num(int dig_cant, int dig_num)
{   /** sumatoria de un digito por su posicion **/
    long int salida;
    int i;
    salida = 0;
    for(i = 0; i < dig_cant; i++)
    {
        salida = salida + (dig_num * potencia(10, i));
    }
    return salida;  /** 1+10+100+1000+10000=11111 **/
}
int desgrana_numero(int entrada,int pos)    /** 562, 2 => 6**/
{
    int i = 0, dig;
    while(i < pos)
    {
        dig = entrada % 10;
        entrada =  entrada / 10;
        i++;
    }
    return dig;
}
int cuenta_dig(unsigned long numero)
{
    int cant_dig;
    cant_dig = 0;
    while(numero != 0)
    {
        cant_dig++;
        numero = numero / 10;
    }
    return cant_dig;
}
int err_entrada(long int intro)
{
    int band, dig;
    band = 0;
    while(intro != 0 && band == 0)
    {
        dig = intro % 10;
        if(dig != 1 && dig != 0)
        {
            band = 1;
        }
        intro =  intro / 10;
    }
    if(band == 0)
    {
        return 0;   /** todos fueron 1 y 0 **/
    }
    return 1;   /** hubo error **/
}
long int codifica_numero(long int entrada)
{
    long int codificado, armado;
    int i, dig, cont_1, cont_0;
    cont_1 = 0, cont_0 = 0, armado = 0;
    
    i = cuenta_dig(entrada);
    while( i >= 0)
    {
        if(i > 0)
        {   /** evita que revise una posicion cero **/
            dig = desgrana_numero(entrada, i);
        }
        else
        {   /** este Else asegura que se inserte el ultimo par **/
            if(cont_0 > 0)
            {
                dig = 1;
            }
            if(cont_1 > 0)
            {
                dig = 0;
            }
        }
        if(dig == 1)
        {   /** cuando aparece un 1 guarda la cantidad de ceros, primero haciendo un corrimiento **/
            cont_1++;
            if(cont_0 > 0)
            {
                codificado = cont_0 * 10;   /** 2 ceros ->  20, 2 * 10  = 20 **/
                armado = armado * 100;  /** si armado =111, 111 * 100 = 11100, me hace espacio con 2 ceros **/
                armado = armado + codificado;   /**  11100 + 20 = 11120 **/
                cont_0 = 0;
            }
        }
        if(dig == 0)
        {   /** cuando aparece un 0 guarda la cantidad de unos, primero haciendo un corrimiento **/
            cont_0++;
            if(cont_1 > 0)
            {
                codificado = cont_1 * 10 + 1;   /** 3 unos ->  31, 3*10 + 1 = 31 **/
                armado = armado * 100;
                armado = armado + codificado;
                cont_1 = 0;
            }
        }
        i--;
    }
    return armado;
}
int menu()
{
    int opcion;
    printf("\n 1- Para ingresar binario");
    printf("\n 2- Para ingresar codificado");
    printf("\n Para salir 0");
    printf("\n Ingrese opcion:    >>>");
    scanf("%d", &opcion);
    return opcion;
}
long int decodifica_num(long int numero)
{   /** se codifica los 2 numeros seguidos, luego se corre las posiciones necesarias y se arma **/
    long int codificado, armado;
    int i, dig_cant, dig_num;
    armado = 0;
    i = cuenta_dig(numero);
    while(i > 0)
    {   /** desgrana saca el digito, segun la posicion dada por i **/
        dig_cant = desgrana_numero(numero, i);  /** 31 **/
        i--;
        dig_num = desgrana_numero(numero, i);
        i--;
        codificado = desenrrolla_num(dig_cant, dig_num); /** 3 , 1   ->  111 **/
        armado = armado * potencia(10, dig_cant); 
        armado = armado + codificado;   /**  0 + 111 para la primer vuelta**/
    }
    return armado;
}
