/**
TP03-EJ04- Construir una calculadora que trabaje solo con fracciones y que permita realizar las
siguientes operaciones:
a) Operaciones de lectura y escritura de fracciones en la entrada/salida estandar.
b) Operaciones aritmeticas entre fracciones: suma, incremento (de una fraccion con otra), resta,
decremento (de una fraccion con otra), producto, cociente y simplificacion de una fracci�n a
irreducible.
c) Operaciones de comparacion de fracciones, tales como: igualdad, mayor, menor, mayor o igual y
menor o igual.
Crear un programa que mediante un menu de opciones permita trabajar con diferentes operaciones
**/
#include <stdio.h>

int menu(void);
void ingresa_fraccion (int *, int *);   /** numerador, denominador **/
void muestra_fraccion (int, int);
void suma_fraccion (int, int, int, int, int *, int *);    /** entrada, salida **/
void resta_fraccion (int, int, int, int, int *, int *);
void mult_fraccion (int, int, int, int, int *, int *);
void div_fraccion (int, int, int, int, int *, int *);
void simplifica_fraccion (int *, int *);
void compara_fracciones(int, int, int, int);

int main (void)
{
    int a, b, c, d, m, p, opcion;
    do
    {
        opcion = menu();
        switch(opcion)
        {
            case 0:
                printf("\n Saliendo del programa...");
            break;
            case 1:
                printf("\n Ingresa primer fraccion: ");
                ingresa_fraccion(&a, &b);
                printf("\n-_-_-_-_-_-_-_-_-_-_-_-\n");
                printf("\n Ingresa segunda fraccion: ");
                ingresa_fraccion(&c, &d);
                suma_fraccion(a, b, c, d, &m, &p);
                simplifica_fraccion(&m, &p);
                printf("\n Resultado: %d / %d", m, p);
            break;
            case 2:
                printf("\n Ingresa primer fraccion: ");
                ingresa_fraccion(&a, &b);
                printf("\n-_-_-_-_-_-_-_-_-_-_-_-\n");
                printf("\n Ingresa segunda fraccion: ");
                ingresa_fraccion(&c, &d);
                resta_fraccion(a, b, c, d, &m, &p);
                printf("\n Resultado: %d / %d", m, p);
            break;
            case 3:
                printf("\n Ingresa primer fraccion: ");
                ingresa_fraccion(&a, &b);
                printf("\n-_-_-_-_-_-_-_-_-_-_-_-\n");
                printf("\n Ingresa segunda fraccion: ");
                ingresa_fraccion(&c, &d);
                mult_fraccion(a, b, c, d, &m, &p);
                printf("\n Resultado: %d / %d", m, p);
            break;
            case 4:
                printf("\n Ingresa primer fraccion: ");
                ingresa_fraccion(&a, &b);
                printf("\n-_-_-_-_-_-_-_-_-_-_-_-\n");
                printf("\n Ingresa segunda fraccion: ");
                ingresa_fraccion(&c, &d);
                div_fraccion(a, b, c, d, &m, &p);
                printf("\n Resultado: %d / %d", m, p);
            break;
            case 5:
                printf("\n Ingrese fraccion a simplificar");
                ingresa_fraccion(&a, &b);
                simplifica_fraccion(&a, &b);
                printf("\n Resultado: %d / %d", a, b);
            break;
            case 6:
                printf("\n Ingresa primer fraccion: ");
                ingresa_fraccion(&a, &b);
                printf("\n-_-_-_-_-_-_-_-_-_-_-_-\n");
                printf("\n Ingresa segunda fraccion: ");
                ingresa_fraccion(&c, &d);
                compara_fracciones(a, b, c, d);
            break;
            default:    printf("\n Opcion incorrecta...");
        }
    }while(opcion != 0);
    
    return 0;
}
int menu(void)
{
    int opcion;
    printf("\n 0- Salir del programa.");
    printf("\n 1- Suma de 2 fracciones.");
    printf("\n 2- Resta de 2 fracciones.");
    printf("\n 3- Producto de 2 fracciones.");
    printf("\n 4- Cociente de 2 fracciones.");
    printf("\n 5- Simplificacion de una fraccion.");
    printf("\n 6- Comparacion de fracciones.\n > > > ");
    scanf("%d", &opcion);
    return opcion;
}
void ingresa_fraccion (int * numerador, int * denominador)
{
    printf("\n Ingrese numerador: ");
    scanf("%d", numerador);
    printf("\n Ingrese denominador: ");
    scanf("%d", denominador);
}
void muestra_fraccion (int a, int b)
{
    printf("\n %d / %d", a, b);
}
void suma_fraccion (int num_A, int den_A, int num_B, int den_B, int * num_res, int * den_res)
{
    * den_res = den_A * den_B;
    * num_res = (num_A * den_B) + (num_B * den_A);
}
void resta_fraccion (int num_A, int den_A, int num_B, int den_B, int * num_res, int * den_res)
{
    * den_res = den_A * den_B;
    * num_res = (num_A * den_B) - (num_B * den_A);
}
void mult_fraccion (int num_A, int den_A, int num_B, int den_B, int * num_res, int * den_res)
{
    * num_res = num_A * num_B;
    * den_res = den_A * den_B;
}
void div_fraccion (int num_A, int den_A, int num_B, int den_B, int * num_res, int * den_res)
{
    * num_res = num_A * den_B;
    * den_res = num_B * den_A;
}
void simplifica_fraccion (int * num_A, int * den_A)
{   /** algoritmo de Euclides **/
    int may, men, resto;
    if(* num_A > * den_A)
    {
        may = * num_A;
        men = * den_A;
    }
    else
    {
        may = * den_A;
        men = * num_A;
    }
    resto = may % men;
    while(resto != 0)
    {
        may = men;
        men = resto;
        resto = may % men;
    }
    * num_A = * num_A / men;
    * den_A = * den_A / men;
}
void compara_fracciones(int num_A, int den_A, int num_B, int den_B)
{
    int den_aux_A;
    den_aux_A = den_A;
    
    num_A = num_A * den_B;
    den_A = den_A * den_B;
    num_B = num_B * den_aux_A;
    den_B = den_B * den_aux_A;
    if(num_A > num_B)
    {
        printf("\n La primera fraccion es mayor a la segunda.");
    }
    if(num_A < num_B)
    {
        printf("\n La primera fraccion es menor a la segunda.");
    }
    if(num_A == num_B)
    {
        printf("\n Son iguales.");
    }
}
