/**
TP03-EJ01- Modificar el ejercicio 2 del TP de nivel 1 para que la transformación de segundos a Hora,
Minutos y segundos sea un módulo.
L____>>>>
    TP01-EJ02- Calcular y mostrar la cantidad de horas, minutos y segundos que existen en una cantidad de segundos ingresada por el usuario.
**/
#include <stdio.h>

void calc_hr_mn_seg(long int, int *, int *, int *);

int main (void)
{
    int hora, min, seg;
    long int entrada_seg;
    
    printf(" Ingrese la cantidad de segundo:  ");
    scanf("%ld", &entrada_seg);
    calc_hr_mn_seg(entrada_seg, &hora, &min, &seg);
    printf("\n Cantidad de horas: %d  -  cantidad de minutos: %d  -  cantidad de segundos: %d", hora, min, seg);
    return 0;
}

void calc_hr_mn_seg(long int entrada, int * hora, int * min, int * seg)
{
    * hora = entrada / 3600;
    entrada = entrada % 3600;
    * min = entrada / 60;
    entrada = entrada % 60;
    * seg = entrada;
}
