/**
TP03-EJ02- Crear un modulo que manipule dos numeros enteros quitando la ultima cifra del primero y
anhadiendola al final del segundo. Realizar un programa que ultilice dicho modulo para invertir un numero.
**/
#include <stdio.h>

void swapper_dig (int *, int *);

int main (void)
{
    int original, capicua;
    original = -3567, capicua = 0;
    while(original != 0)
    {
        swapper_dig(&original, &capicua);
    }
    printf("%d numero original \n", original);
    printf("%d numero capicua, original invertido", capicua);
    return 0;
}
void swapper_dig (int * primero, int * segundo)
{   /** funciona con numeros negativos **/
    * segundo = * segundo * 10;
    * segundo = * segundo + (* primero % 10);
    * primero = * primero / 10;
}
