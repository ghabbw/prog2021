/**
TP03-EJ03- Implementar un modulo que reciba dos variables enteras e intercambie sus valores. Luego, 
utilizando el modulo realizado, implementar un programa que reciba tres variables de tipo entero y los 
reasigne de forma tal que a < b < c
**/
#include <stdio.h>

void swapper_num (int *, int *);

int main (void)
{
    int a, b, c;
    printf("\n Ingrese valor de a = ");
    scanf("%d", &a);
    printf("\n Ingrese valor de b = ");
    scanf("%d", &b);
    printf("\n Ingrese valor de c = ");
    scanf("%d", &c);
    if(a > b)
    {
        swapper_num(&a, &b);
    }
    if(b > c)
    {
        swapper_num(&b, &c);
    }
    if(a > b)
    {
        swapper_num(&a, &b);
    }
    printf("a = %d < b = %d < c = %d", a, b, c);
    return 0;
}
void swapper_num (int * primero, int * segundo)
{   /** funciona con numeros negativos **/
    int aux;
    aux = * segundo;
    * segundo = * primero;
    * primero = aux;
}
